/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Fichero: ModeloFichero.java
 *
 * @author Jose Alvarez Gallego <crackmaximo@hotmail.com>
 * @date 26-ene-2015
 */
public class ModeloFichero implements Modelo {

    int id = 0;
    File fi = new File("1415ceed111prgt6e1.csv");

    @Override
    public void create(Alumno alumno) {
        try {
            FileWriter fw = new FileWriter(fi, true);
            fw.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getEdad() + ";" + alumno.getEmail() + ";\r\n");
            fw.close();
            id++;
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public HashSet read() {
        HashSet hs = new HashSet();
        Alumno alumno = new Alumno();
        try {
            FileReader fr = new FileReader(fi);
            BufferedReader br = new BufferedReader(fr);
            String s;
            s = br.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String id = st.nextToken();
                String nombre = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                String email = st.nextToken();
                alumno = new Alumno(id, nombre, edad, email);
                hs.add(alumno);
                s = br.readLine();
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hs;
    }

    @Override
    public void update(Alumno alumno) {
        File fo = new File("temporal.csv");
        Alumno alu;
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(fi);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String id = st.nextToken();
                String nombre = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                String email = st.nextToken();
                alu = new Alumno(id, nombre, edad, email);
                if (alumno.getId().equals(id)) {
                    ft.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getEdad() + ";" + alumno.getEmail() + ";\r\n");
                    
                } else {
                    ft.write(alu.getId() + ";" + alu.getNombre() + ";" + alu.getEdad() + ";" + alu.getEmail() + ";\r\n");
                }

                s = bf.readLine();

            }
            ft.close();
            fr.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        fi.delete();
        boolean renonmbre = fo.renameTo(new File("1415ceed111prgt6e1.csv"));

    }

    @Override
    public void delete(Alumno alumno) {
        File fo = new File("temporal.csv");
        Alumno alu;
        try {
            FileWriter ft = new FileWriter(fo, true);
            FileReader fr = new FileReader(fi);
            BufferedReader bf = new BufferedReader(fr);
            String s;
            s = bf.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                String id = st.nextToken();
                String nombre = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                String email = st.nextToken();
                alu = new Alumno(id, nombre, edad, email);
                if (!alumno.getId().equals(id)) {
                    ft.write(alu.getId() + ";" + alu.getNombre() + ";" + alu.getEdad() + ";" + alu.getEmail() + ";\r\n");
                } 
                s = bf.readLine();
            }
            ft.close();
            fr.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        fi.delete();
        boolean renonmbre = fo.renameTo(new File("1415ceed111prgt6e1.csv"));
   
    }

    @Override
    public int getId() {
        return id;
    }

}
