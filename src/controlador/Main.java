
package controlador;

import java.io.IOException;
import modelo.Modelo;
import vista.Vista;

/**
 * Fichero: Main.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Main {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws IOException {

    // Declaramos el modelo.  
    Modelo m = null;

    // Declaramos la vista y creamos objeto vista 
    Vista v = new Vista();

    // Declaramos y creamos un objeto controlador.
    Controlador c = new Controlador(m, v);

  }

}
